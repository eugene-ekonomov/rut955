import java.nio.ByteBuffer;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Decoder {

    public static List<LocMessage> decode(ByteBuffer buf) {
        // todo осталась самая малость...
        List<LocMessage> locMessages = new ArrayList<>();
        byte codecId = buf.get();
        if(codecId!=8){
            return Collections.emptyList();
        }
        byte numberOfData = buf.get();
        for(int i = 0; i < numberOfData; i++){
            LocMessage locMessage = new LocMessage();
            locMessage.setDateTime(ZonedDateTime.ofInstant(Instant.ofEpochMilli(buf.getLong()), ZoneId.of("Z")));
            buf.get();//priority
            locMessage.setLongitude( buf.getInt() / 10_000_000.0);
            locMessage.setLatitude( buf.getInt() / 10_000_000.0);
            locMessage.setAltitude((int) buf.getShort());
            locMessage.setAngle((int) buf.getShort());
            locMessage.setSatellites((int) buf.get());
            locMessage.setSpeed((int) buf.getShort());

            buf.get();//IO Element ID of Event
            short size = buf.get();
            short size1 = buf.get();
            parseIo(1, size1, buf, locMessage);
            short size2 = buf.get();
            parseIo(2, size2, buf, locMessage);
            short size4 = buf.get();
            parseIo(4, size4, buf, locMessage);
            short size8 = buf.get();
            parseIo(8, size8, buf, locMessage);

            locMessages.add(locMessage);
        }

        return locMessages;
    }
    private static void parseIo(int valueLength, short size, ByteBuffer buf, LocMessage locMessage){
        for(int j = 0; j < size; j++){
            short ioElementId = buf.get();
            long value=0;
            switch(valueLength){
                case 1:
                    value = buf.get();
                    break;
                case 2:
                    value = buf.getShort();
                    break;
                case 4:
                    value = buf.getInt();
                    break;
                case 8:
                    value = buf.getLong();
                    break;
            }
            setIoElement(locMessage, ioElementId, value);
        }
    }
    private static void setIoElement(LocMessage locMessage, short id, long value){
        switch(id){
            case 1:
                locMessage.setDigitalInputStatus1(value==1?true:false);
                break;
            case 2:
                locMessage.setDigitalInputStatus2(value==1?true:false);
                break;
            case 9:
                locMessage.setAnalogInput1((double) value);
                break;
            case 21:
                locMessage.setGsmLevel((int) value);
                break;
        }
    }
}
